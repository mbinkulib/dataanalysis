package DataAnalysis;

public class Metrics 
{

	private double commitLatency;
	private double finalizedLatency;
	private double effectiveThroughput;
	private double overallThroughput;
	private int noCommittedTransactions;
	private int noFinalizedTransactions;
	private int hostIndex;
	
	public Metrics() 
	{
		commitLatency = -1;
		finalizedLatency = -1;
		effectiveThroughput = -1;
		overallThroughput = -1;
		noCommittedTransactions = -1;
		noFinalizedTransactions = -1;
		hostIndex = -1;
	}
	
	public double getCommitLatency()
	{
		return this.commitLatency;
	}
	
	public double getFinalizedLatency()
	{
		return this.finalizedLatency;
	}
	
	public double getEffectiveThroughput()
	{
		return this.effectiveThroughput;
	}
	
	public double getOverallThroughput()
	{
		return this.overallThroughput;
	}
	
	public int getNoCommittedTransactions()
	{
		return this.noCommittedTransactions;
	}
	
	public int getNoFinalizedTransactions()
	{
		return this.noFinalizedTransactions;
	}

	public void setCommitLatency(double cmtLatency)
	{
		this.commitLatency = cmtLatency;
	}
	
	public void setFinalizedLatency(double finLatency)
	{
		this.finalizedLatency = finLatency;
	}
	
	public void setEffectiveThroughput(double effThroughput)
	{
		this.effectiveThroughput = effThroughput;
	}
	
	public void setOverallThroughput(double ovThroughput)
	{
		this.overallThroughput = ovThroughput;
	}
	
	public void setNoCommittedTransactions(int noCmtTransactions)
	{
		this.noCommittedTransactions = noCmtTransactions;
	}
	
	public void setNoFinalizedTransactions(int noFinTransactions)
	{
		this.noFinalizedTransactions = noFinTransactions;
	}
	
	public int getHostIndex()
	{
		return this.hostIndex;
	}
	
	public void setHostIndex(int hostIndx)
	{
		this.hostIndex = hostIndx;
	}
	
	public double getSuccessProbability()
	{
		return this.getNoCommittedTransactions()/10000*100;
	}

}
