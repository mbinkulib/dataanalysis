package DataAnalysis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
//import java.util.Vector;

public class readOverallThroughtput {
	// static String rates_folder_name;
	// static Vector<String> hosts_IPS;

	public static void readOverallThroughput(int s, int r) throws IOException {

		File overallThroughputFile1, overallThroughputFile2, overallThroughputFile3, overallThroughputFile4;
		FileOutputStream FOSoverallThroughput1, FOSoverallThroughput2, FOSoverallThroughput3, FOSoverallThroughput4;
		BufferedWriter BWoverallThroughput1 = null, BWoverallThroughput2 = null, BWoverallThroughput3 = null,
				BWoverallThroughput4 = null;

		// int[] host_sets = {12}; // {2, 4, 8, 12, 16};
		// int[] rates = {500};// {50, 500, 1000, 1500};

		double ab_min = 10000.0, ab_max = 0.0;

		// for (int s = 0; s < host_sets.length; s++)
		// {
		System.out.println("Set#: " + (s + 1) + ", set value is: " + s);
		// hosts_IPS = ExtractMetrics.readIPS(host_sets[s]);

		// for (int r = 0; r < rates.length; r++)
		// {
		String rates_folder_name = "/exp_" + s + "_servers_1_threads_" + r + "_rates_4_clients_flex/"; // host_128.110.153.127_4_5
		System.out.println(rates_folder_name);
		double avg = 0; // , avg_all=0;
		int devBy = 0; // devBy = 35;

		overallThroughputFile1 = new File(ExtractMetrics.analysis_dir + rates_folder_name + "overallThroughputAv1_"
				+ s + "_" + r + ".txt");
		FOSoverallThroughput1 = new FileOutputStream(overallThroughputFile1);
		BWoverallThroughput1 = new BufferedWriter(new OutputStreamWriter(FOSoverallThroughput1));

		overallThroughputFile2 = new File(ExtractMetrics.analysis_dir + rates_folder_name + "overallThroughputAv2_"
				+ s + "_" + r + ".txt");
		FOSoverallThroughput2 = new FileOutputStream(overallThroughputFile2);
		BWoverallThroughput2 = new BufferedWriter(new OutputStreamWriter(FOSoverallThroughput2));

		overallThroughputFile3 = new File(ExtractMetrics.analysis_dir + rates_folder_name + "overallThroughputAv3_"
				+ s + "_" + r + ".txt");
		FOSoverallThroughput3 = new FileOutputStream(overallThroughputFile3);
		BWoverallThroughput3 = new BufferedWriter(new OutputStreamWriter(FOSoverallThroughput3));

		overallThroughputFile4 = new File(ExtractMetrics.analysis_dir + rates_folder_name + "overallThroughputAv4_"
				+ s + "_" + r + ".txt");
		FOSoverallThroughput4 = new FileOutputStream(overallThroughputFile4);
		BWoverallThroughput4 = new BufferedWriter(new OutputStreamWriter(FOSoverallThroughput4));

		System.out.println("Reading overall throughput");
		for (int i = 1; i <= 4; i++) {
			File file = new File(
					ExtractMetrics.analysis_dir + rates_folder_name + "overallThroughputClient" + (i) + ".txt");
			try {
				@SuppressWarnings("resource")
				BufferedReader br = new BufferedReader(new FileReader(file));
				String st;
				while ((st = br.readLine()) != null) 
				{
					if (Double.parseDouble(st) == 0.0)
						continue;
					devBy++;
					System.out.println(st);
					double val = Double.parseDouble(st);
					ab_min = Math.min(ab_min, val);
					ab_max = Math.max(ab_max, val);
					if (val > 0)
						avg = avg + val;
					// else
					// devBy --;
				}
				System.out.println("avg of " + i + "= " + (avg / devBy) + " which is devided by " + devBy);

				switch (i) {
				case 1:
					BWoverallThroughput1.write((avg / devBy) + "");
					BWoverallThroughput1.newLine();
					break;
				case 2:
					BWoverallThroughput2.write((avg / devBy) + "");
					BWoverallThroughput2.newLine();
					break;
				case 3:
					BWoverallThroughput3.write((avg / devBy) + "");
					BWoverallThroughput3.newLine();
					break;
				case 4:
					BWoverallThroughput4.write((avg / devBy) + "");
					BWoverallThroughput4.newLine();
					break;
				}

				// avg_all=avg_all+(avg/devBy);
				avg = 0;
				devBy = 0;
			} catch (IOException e) {
				e.printStackTrace();
			}
			// devBy = 35;
		}
		// System.out.println("avg of all 4 = "+(avg_all/4));
		// BWoverallThroughput.write((avg_all/4)+"");
		// BWoverallThroughput.newLine();
		// }
		BWoverallThroughput1.close();
		BWoverallThroughput2.close();
		BWoverallThroughput3.close();
		BWoverallThroughput4.close();
		// hosts_IPS = null;
		// }

		System.out.println("ab_min: " + ab_min);
		System.out.println("ab_max: " + ab_max);
	}

}
