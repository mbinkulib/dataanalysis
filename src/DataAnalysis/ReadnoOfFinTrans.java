package DataAnalysis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Vector;

public class ReadnoOfFinTrans {
	
	static String rates_folder_name;
	static Vector<String> hosts_IPS;

	public static void main(String[] args) throws IOException
	{
		
		File noFinalizedTransactionsFile;
		FileOutputStream FOSnoFinalizedTransactions;
		BufferedWriter BWnoFinalizedTransactions = null;
		
		int[] host_sets = {4}; // {2, 4, 8, 12, 16};
		int[] rates =  {500};// {50, 500, 1000, 1500};
		
		double ab_min = 10000.0, ab_max = 0.0;
		
		for (int s = 0; s < host_sets.length; s++) 
		{
			//System.out.println("Set#: " + (s + 1) + ", set value is: " + host_sets[s]);
			hosts_IPS = ExtractMetrics.readIPS(host_sets[s]);
			//System.out.println(hosts_IPS);

			for (int r = 0; r < rates.length; r++) 
			{
				rates_folder_name = "/exp_" + host_sets[s] + "_servers_4_threads_" + rates[r] + "_rates/"; // host_128.110.153.127_4_5
				System.out.println(rates_folder_name);
				
				noFinalizedTransactionsFile = new File(ExtractMetrics.analysis_dir + rates_folder_name + "noFinalizedTransactionsSum_"+host_sets[s]+"_"+rates[r]+".txt");
				FOSnoFinalizedTransactions = new FileOutputStream(noFinalizedTransactionsFile);
				BWnoFinalizedTransactions = new BufferedWriter(new OutputStreamWriter(FOSnoFinalizedTransactions));
				
				System.out.println("Reading number of finalized transactions");
				for (int i = 1; i <=6; i++) 
				{
					File file = new File(ExtractMetrics.analysis_dir + rates_folder_name + "noFinalizedTransactionsClient"+(i)+".txt"); 
					System.out.print("Client: "+i+", with ");
					int count = 35;
					try 
					{
					  @SuppressWarnings("resource")
					  BufferedReader br = new BufferedReader(new FileReader(file)); 
					  String st; 
					  while ((st = br.readLine()) != null) 
					  {
						    //System.out.println(st);
						    int val = Integer.parseInt(st);
						    ab_min = Math.min(ab_min, val);
						    ab_max = Math.max(ab_max, val);
						    BWnoFinalizedTransactions.write(val+"");
						    BWnoFinalizedTransactions.newLine();
						    if (val != 10000)
						    	count--;
					  }
					  System.out.println("count: "+count);

					} 
					catch (IOException e) 
					{
						e.printStackTrace();
					} 
				} 
			}
			hosts_IPS = null;
		}
		BWnoFinalizedTransactions.close();
		System.out.println("ab_min: "+ab_min);
		System.out.println("ab_max: "+ab_max);

	}

}
