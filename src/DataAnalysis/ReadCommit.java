package DataAnalysis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
//import java.util.Vector;

public class ReadCommit {
	

	public static void readCommitLatency(int s, int r) throws IOException {
		
		File commitLatencyFile1, commitLatencyFile2, commitLatencyFile3, commitLatencyFile4;
		FileOutputStream FOScommitLatency1, FOScommitLatency2, FOScommitLatency3, FOScommitLatency4;
		BufferedWriter BWcommitLatency1 = null, BWcommitLatency2 = null, BWcommitLatency3 = null,
				BWcommitLatency4 = null;

		System.out.println("Set#: " + (s + 1) + ", set value is: " + s);
		//Vector<String> hosts_IPS = ExtractMetrics.readIPS(s);
		String rates_folder_name = "/exp_" + s + "_servers_1_threads_" + r + "_rates_4_clients_flex/"; // host_128.110.153.127_4_5
		System.out.println(rates_folder_name);
		double avg = 0; // avg_all = 0;
		// int devBy = 35;
		int devBy = 0;
		commitLatencyFile1 = new File(ExtractMetrics.analysis_dir + rates_folder_name + "commitLatencyAv1_"
				+ s + "_" + r + ".txt");
		commitLatencyFile2 = new File(ExtractMetrics.analysis_dir + rates_folder_name + "commitLatencyAv2_"
				+ s + "_" + r + ".txt");
		commitLatencyFile3 = new File(ExtractMetrics.analysis_dir + rates_folder_name + "commitLatencyAv3_"
				+ s + "_" + r + ".txt");
		commitLatencyFile4 = new File(ExtractMetrics.analysis_dir + rates_folder_name + "commitLatencyAv4_"
				+ s + "_" + r + ".txt");

		FOScommitLatency1 = new FileOutputStream(commitLatencyFile1);
		BWcommitLatency1 = new BufferedWriter(new OutputStreamWriter(FOScommitLatency1));

		FOScommitLatency2 = new FileOutputStream(commitLatencyFile2);
		BWcommitLatency2 = new BufferedWriter(new OutputStreamWriter(FOScommitLatency2));

		FOScommitLatency3 = new FileOutputStream(commitLatencyFile3);
		BWcommitLatency3 = new BufferedWriter(new OutputStreamWriter(FOScommitLatency3));

		FOScommitLatency4 = new FileOutputStream(commitLatencyFile4);
		BWcommitLatency4 = new BufferedWriter(new OutputStreamWriter(FOScommitLatency4));

		System.out.println("Reading commit latency");
		// for each client
		for (int i = 1; i <= 4; i++) 
		{
			File file = new File(
					ExtractMetrics.analysis_dir + rates_folder_name + "commitLatencyClient" + (i) + ".txt");
			try 
			{
				@SuppressWarnings("resource")
				BufferedReader br = new BufferedReader(new FileReader(file));
				String st;
				// read from 1-35 values
				while ((st = br.readLine()) != null) 
				{
					if (Double.parseDouble(st) == 0.0)
						continue;
					devBy++;
					double val = Double.parseDouble(st);
					System.out.println(val);
					if (val > 0) {
						avg = avg + val;
						System.out.println("avg: " + avg);
					}

					// else
					// devBy--;
				}
				// calculate the average
				System.out.println("avg of client" + i + "= " + (avg / devBy) + " which is devided by " + devBy);

				// write the average in the right new file
				switch (i) {
				case 1:
					BWcommitLatency1.write((avg / devBy) + "");
					BWcommitLatency1.newLine();
					break;
				case 2:
					BWcommitLatency2.write((avg / devBy) + "");
					BWcommitLatency2.newLine();
					break;
				case 3:
					BWcommitLatency3.write((avg / devBy) + "");
					BWcommitLatency3.newLine();
					break;
				case 4:
					BWcommitLatency4.write((avg / devBy) + "");
					BWcommitLatency4.newLine();
					break;
				}

				// avg_all = avg_all + (avg / devBy);
				avg = 0;
				devBy = 0; // devBy = 35;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		// System.out.println("avg of all 4 = " + (avg_all / 4));

		BWcommitLatency1.close();
		BWcommitLatency2.close();
		BWcommitLatency3.close();
		BWcommitLatency4.close();
		//hosts_IPS = null;

	}

}
