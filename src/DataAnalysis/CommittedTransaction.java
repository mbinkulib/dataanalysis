package DataAnalysis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class CommittedTransaction {
	
public static void readCommitTsx(int s, int r) throws IOException {
		
		File noCommitTsxFile1, noCommitTsxFile2, noCommitTsxFile3, noCommitTsxFile4;
		FileOutputStream FOSnoCommitTsxFile1, FOSnoCommitTsxFile2, FOSnoCommitTsxFile3, FOSnoCommitTsxFile4;
		BufferedWriter BWnoCommitTsx1 = null, BWnoCommitTsx2 = null, BWnoCommitTsx3 = null,
				BWnoCommitTsx4 = null;

		System.out.println("Set#: " + (s + 1) + ", set value is: " + s);
		//Vector<String> hosts_IPS = ExtractMetrics.readIPS(s);
		String rates_folder_name = "/exp_" + s + "_servers_1_threads_" + r + "_rates_4_clients_flex/"; // host_128.110.153.127_4_5
		System.out.println(rates_folder_name);
		double avg = 0; // avg_all = 0;
		// int devBy = 35;
		int devBy = 0;
		noCommitTsxFile1 = new File(ExtractMetrics.analysis_dir + rates_folder_name + "noCommittedTransactionsAv1_"
				+ s + "_" + r + ".txt");
		noCommitTsxFile2 = new File(ExtractMetrics.analysis_dir + rates_folder_name + "noCommittedTransactionsAv2_"
				+ s + "_" + r + ".txt");
		noCommitTsxFile3 = new File(ExtractMetrics.analysis_dir + rates_folder_name + "noCommittedTransactionsAv3_"
				+ s + "_" + r + ".txt");
		noCommitTsxFile4 = new File(ExtractMetrics.analysis_dir + rates_folder_name + "noCommittedTransactionsAv4_"
				+ s + "_" + r + ".txt");

		FOSnoCommitTsxFile1 = new FileOutputStream(noCommitTsxFile1);
		BWnoCommitTsx1 = new BufferedWriter(new OutputStreamWriter(FOSnoCommitTsxFile1));

		FOSnoCommitTsxFile2 = new FileOutputStream(noCommitTsxFile2);
		BWnoCommitTsx2 = new BufferedWriter(new OutputStreamWriter(FOSnoCommitTsxFile2));

		FOSnoCommitTsxFile3 = new FileOutputStream(noCommitTsxFile3);
		BWnoCommitTsx3 = new BufferedWriter(new OutputStreamWriter(FOSnoCommitTsxFile3));

		FOSnoCommitTsxFile4 = new FileOutputStream(noCommitTsxFile4);
		BWnoCommitTsx4 = new BufferedWriter(new OutputStreamWriter(FOSnoCommitTsxFile4));

		System.out.println("Reading commit latency");
		// for each client
		for (int i = 1; i <= 4; i++) {
			File file = new File(
					ExtractMetrics.analysis_dir + rates_folder_name + "noCommittedTransactionsClient" + (i) + ".txt");
			try {
				@SuppressWarnings("resource")
				BufferedReader br = new BufferedReader(new FileReader(file));
				String st;
				// read from 1-35 values
				while ((st = br.readLine()) != null) 
				{
					if (Double.parseDouble(st) == 0.0)
						continue;
					devBy++;
					double val = Double.parseDouble(st);
					System.out.println(val);
					if (val > 0) {
						avg = avg + val;
						System.out.println("avg: " + avg);
					}

					// else
					// devBy--;
				}
				// calculate the average
				System.out.println("avg of client" + i + "= " + (avg / devBy) + " which is devided by " + devBy);

				// write the average in the right new file
				switch (i) {
				case 1:
					BWnoCommitTsx1.write((avg / devBy) + "");
					BWnoCommitTsx1.newLine();
					break;
				case 2:
					BWnoCommitTsx2.write((avg / devBy) + "");
					BWnoCommitTsx2.newLine();
					break;
				case 3:
					BWnoCommitTsx3.write((avg / devBy) + "");
					BWnoCommitTsx3.newLine();
					break;
				case 4:
					BWnoCommitTsx4.write((avg / devBy) + "");
					BWnoCommitTsx4.newLine();
					break;
				}

				// avg_all = avg_all + (avg / devBy);
				avg = 0;
				devBy = 0; // devBy = 35;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		// System.out.println("avg of all 4 = " + (avg_all / 4));

		BWnoCommitTsx1.close();
		BWnoCommitTsx2.close();
		BWnoCommitTsx3.close();
		BWnoCommitTsx4.close();
		//hosts_IPS = null;

	}

}
