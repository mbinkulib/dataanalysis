package DataAnalysis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Vector;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.input.ReversedLinesFileReader;

public class ExtractMetrics {

	static final String results_path = "/home/blockchains/eclipse-workspace/DataAnalysis/FinalResults/Raft/";
	public static final String analysis_dir = "/home/blockchains/eclipse-workspace/DataAnalysis/FinalResults/Summary/Raft/";
	static String rates_folder_name;
	static Vector<String> hosts_IPS;

	public static void main(String[] args) throws IOException {
		int[] host_sets = { 8 }; // {2, 4, 8, 12, 16};
		int[] rates = { 50 };// {50, 500, 1000, 1500, 2000, 5000, 7500, 9000};

		for (int s = 0; s < host_sets.length; s++) {
			System.out.println("Set#: " + (s + 1) + ", set value is: " + host_sets[s]);
			hosts_IPS = readIPS(host_sets[s]);

			for (int r = 0; r < rates.length; r++) {
				rates_folder_name = "/exp_" + host_sets[s] + "_servers_1_threads_" + rates[r]
						+ "_rates_4_clients_flex/"; // host_128.110.153.127_4_5
				System.out.println(rates_folder_name);

				List<Vector<Metrics>> list = new ArrayList<Vector<Metrics>>();

				Vector<Metrics> Client1 = new Vector<Metrics>();
				Client1 = fillOutMetrics(1, Client1);
				list.add(Client1);
				System.out.println("fillOutMetrics done for Client1");

				Vector<Metrics> Client2 = new Vector<Metrics>();
				Client2 = fillOutMetrics(2, Client2);
				list.add(Client2);
				System.out.println("fillOutMetrics done for Client2");

				Vector<Metrics> Client3 = new Vector<Metrics>();
				Client3 = fillOutMetrics(3, Client3);
				list.add(Client3);
				System.out.println("fillOutMetrics done for Client3");

				Vector<Metrics> Client4 = new Vector<Metrics>();
				Client4 = fillOutMetrics(4, Client4);
				list.add(Client4);
				System.out.println("fillOutMetrics done for Client4");

				/*
				 * Vector<Metrics> Client5 = new Vector<Metrics>(); Client5 = fillOutMetrics(5,
				 * Client5); list.add(Client5);
				 * 
				 * Vector<Metrics> Client6 = new Vector<Metrics>(); Client6 = fillOutMetrics(6,
				 * Client6); list.add(Client6);
				 */
				// create mertics files

				new File(analysis_dir + rates_folder_name).mkdir();
				System.out.println("Metrics write to files status: " + writeMetricsToFiles(list));
				list = new ArrayList<Vector<Metrics>>();

				ReadCommit.readCommitLatency(host_sets[s], rates[r]);
				ReadEffThroughput.readEffectiveThrougput(host_sets[s], rates[r]);
				readOverallThroughtput.readOverallThroughput(host_sets[s], rates[r]);
				CommittedTransaction.readCommitTsx(host_sets[s], rates[r]);
			}

		}
		hosts_IPS = null;

	}

	public static int writeMetricsToFiles(List<Vector<Metrics>> list) throws IOException {

		System.out.println("NOW WRITING.................................");
		File hostIndexFile, finalizedLatencyFile, commitLatencyFile, effectiveThroughputFile, overallThroughputFile,
				noCommittedTransactionsFile, noFinalizedTransactionsFile, successProbabilityFile;

		FileOutputStream FOShostIndex, FOSfinalizedLatency, FOScommitLatency, FOSeffectiveThroughput,
				FOSoverallThroughput, FOSnoCommittedTransactions, FOSnoFinalizedTransactions, FOSsuccessProbability;

		BufferedWriter BWhostIndex, BWfinalizedLatency, BWcommitLatency, BWeffectiveThroughput, BWoverallThroughput,
				BWnoCommittedTransactions, BWnoFinalizedTransactions, BWsuccessProbability;

		System.out.println("list.size(): " + list.size());
		for (int i = 0; i < list.size(); i++) {

			// hostIndex
			new File(analysis_dir + rates_folder_name).mkdir();
			hostIndexFile = new File(analysis_dir + rates_folder_name + "hostIndexClient" + (i + 1) + ".txt");
			FOShostIndex = new FileOutputStream(hostIndexFile);
			BWhostIndex = new BufferedWriter(new OutputStreamWriter(FOShostIndex));

			// finalizedLatency

			finalizedLatencyFile = new File(
					analysis_dir + rates_folder_name + "finalizedLatencyClient" + (i + 1) + ".txt");
			FOSfinalizedLatency = new FileOutputStream(finalizedLatencyFile);
			BWfinalizedLatency = new BufferedWriter(new OutputStreamWriter(FOSfinalizedLatency));

			// commitLatency

			commitLatencyFile = new File(analysis_dir + rates_folder_name + "commitLatencyClient" + (i + 1) + ".txt");
			FOScommitLatency = new FileOutputStream(commitLatencyFile);
			BWcommitLatency = new BufferedWriter(new OutputStreamWriter(FOScommitLatency));

			// effectiveThroughput

			effectiveThroughputFile = new File(
					analysis_dir + rates_folder_name + "effectiveThroughputClient" + (i + 1) + ".txt");
			FOSeffectiveThroughput = new FileOutputStream(effectiveThroughputFile);
			BWeffectiveThroughput = new BufferedWriter(new OutputStreamWriter(FOSeffectiveThroughput));

			// overallThroughput

			overallThroughputFile = new File(
					analysis_dir + rates_folder_name + "overallThroughputClient" + (i + 1) + ".txt");
			FOSoverallThroughput = new FileOutputStream(overallThroughputFile);
			BWoverallThroughput = new BufferedWriter(new OutputStreamWriter(FOSoverallThroughput));

			// noCommittedTransactions

			noCommittedTransactionsFile = new File(
					analysis_dir + rates_folder_name + "noCommittedTransactionsClient" + (i + 1) + ".txt");
			FOSnoCommittedTransactions = new FileOutputStream(noCommittedTransactionsFile);
			BWnoCommittedTransactions = new BufferedWriter(new OutputStreamWriter(FOSnoCommittedTransactions));

			// noFinalizedTransactions

			noFinalizedTransactionsFile = new File(
					analysis_dir + rates_folder_name + "noFinalizedTransactionsClient" + (i + 1) + ".txt");
			FOSnoFinalizedTransactions = new FileOutputStream(noFinalizedTransactionsFile);
			BWnoFinalizedTransactions = new BufferedWriter(new OutputStreamWriter(FOSnoFinalizedTransactions));

			// SuccessProbability

			successProbabilityFile = new File(
					analysis_dir + rates_folder_name + "SuccessProbabilityClient" + i + ".txt");
			FOSsuccessProbability = new FileOutputStream(successProbabilityFile);
			BWsuccessProbability = new BufferedWriter(new OutputStreamWriter(FOSsuccessProbability));

			System.out.println("i: " + i);
			for (int j = 0; j < 35; j++) {

				System.out.println("j: " + j);
				System.out.println("Value is: " + list.get(i).get(j).getHostIndex());
				BWhostIndex.write(list.get(i).get(j).getHostIndex());
				BWhostIndex.newLine();

				// BWfinalizedLatency.write(list.get(i).get(j).getFinalizedLatency()+"");
				// BWfinalizedLatency.newLine();

				System.out.println(list.get(i).get(j).getCommitLatency() + "");
				BWcommitLatency.write(list.get(i).get(j).getCommitLatency() + "");
				BWcommitLatency.newLine();

				System.out.println(list.get(i).get(j).getEffectiveThroughput() + "");
				BWeffectiveThroughput.write(list.get(i).get(j).getEffectiveThroughput() + "");
				BWeffectiveThroughput.newLine();

				System.out.println(list.get(i).get(j).getOverallThroughput() + "");
				BWoverallThroughput.write(list.get(i).get(j).getOverallThroughput() + "");
				BWoverallThroughput.newLine();

				System.out.println(list.get(i).get(j).getNoCommittedTransactions() + "");
				BWnoCommittedTransactions.write(list.get(i).get(j).getNoCommittedTransactions() + "");
				BWnoCommittedTransactions.newLine();

				// BWnoFinalizedTransactions.write(list.get(i).get(j).getNoFinalizedTransactions()+"");
				// BWnoFinalizedTransactions.newLine();

				System.out.println(list.get(i).get(j).getSuccessProbability() + "");
				BWsuccessProbability.write(list.get(i).get(j).getSuccessProbability() + "");
				BWsuccessProbability.newLine();
			}

			BWhostIndex.close();
			BWfinalizedLatency.close();
			BWcommitLatency.close();
			BWeffectiveThroughput.close();
			BWoverallThroughput.close();
			BWnoCommittedTransactions.close();
			BWnoFinalizedTransactions.close();
			BWsuccessProbability.close();
		}

		return -1;
	}

	public static Vector<String> readIPS(int noNodes) {
		Vector<String> ips = new Vector<String>();
		int i = 0;

		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(results_path + "ips"));
			String line = reader.readLine();
			ips.add(line);
			i++;
			while (line != null & i < noNodes) {
				line = reader.readLine();
				ips.add(line);
				i++;
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("IPs of hosts are:");
		for (i = 0; i < ips.size(); i++)
			System.out.println(ips.get(i));
		return ips;

	}

	public static boolean canReadMetricsFile(String fileName) throws IOException {
		File f = new File(fileName);
		if (f.isFile())
			return true;
		else
			return false;
	}

	public static List<String> readMetricsFile(String fileName) {
		int numLastLineToRead = 7;
		File file = new File(fileName);

		List<String> result = new ArrayList<>();

		try (ReversedLinesFileReader reader = new ReversedLinesFileReader(file, StandardCharsets.UTF_8)) {

			String line = "";
			while ((line = reader.readLine()) != null && result.size() < numLastLineToRead) {
				result.add(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static double readFinLatency(List<String> metrics) {
		String fin_delay = metrics.get(3).substring(metrics.get(3).indexOf('=') + 2, metrics.get(3).length());
		fin_delay = fin_delay.replaceAll("[a-z]", "");
		System.out.println(fin_delay);
		if (fin_delay.equals("-"))
			return 0;
		return Double.parseDouble(fin_delay);
	}

	public static double readCmtLatency(List<String> metrics) {
		String cmt_delay = metrics.get(4).substring(metrics.get(4).indexOf('=') + 2, metrics.get(4).length());
		cmt_delay = cmt_delay.replaceAll("[a-z]", "");
		System.out.println("Commit Delay: " + cmt_delay);
		double ret = 500;
		if (cmt_delay.contains(":") || cmt_delay.contains("-") || cmt_delay.contains("="))
			return ret;
		else {
			ret = 0.0;
			try {
				ret = Double.parseDouble(cmt_delay);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return ret;
	}

	public static double readOvThroughput(List<String> metrics) {
		int num_ov = metrics.get(0).indexOf('=') + 1;
		String ov_throughput = metrics.get(0).substring(num_ov, metrics.get(0).length());
		ov_throughput = ov_throughput.replaceAll("[a-z]", "");
		ov_throughput = ov_throughput.replaceAll(" ", "");
		System.out.println("Overal Throughput before parsing: " + ov_throughput);
		double oT = 0.0;
		if (ov_throughput.contains(":") || ov_throughput.contains("-") || ov_throughput.contains("="))
			return oT;
		try {
			oT = Double.parseDouble(ov_throughput);
			System.out.println("Overal Throughput: " + oT);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return oT;
	}

	public static double readEffThroughput(List<String> metrics) {
		int num_eff = metrics.get(1).indexOf('=') + 1;
		String eff_throughput = metrics.get(1).substring(num_eff + 1, metrics.get(1).length());
		eff_throughput = eff_throughput.replaceAll("[a-z]", "");
		System.out.println("Effective Throughput: " + eff_throughput);
		// eff_throughput = eff_throughput.substring(eff_throughput.indexOf('='),
		// eff_throughput.length());
		double thru = 0.0;
		if (eff_throughput.contains(":") || eff_throughput.contains("-") || eff_throughput.contains("="))
			return thru;
		try {
			thru = Double.parseDouble(eff_throughput);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return thru;
	}

	public static int readFinTxs(List<String> metrics) {
		String fin_txn = metrics.get(6).substring(metrics.get(6).indexOf('=') + 2, metrics.get(6).length());
		System.out.println("Number of finalized transactions: " + fin_txn);
		int fin = 0;
		if (fin_txn.contains(":") || fin_txn.contains("-") || fin_txn.contains("="))
			return fin;
		try {
			fin = Integer.parseInt(fin_txn);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fin;
	}

	public static int readCmtTxs(List<String> metrics) {
		String cmt_txn = metrics.get(5).substring(metrics.get(5).indexOf('=') + 2, metrics.get(5).length());
		System.out.println("Number of committed transactions: " + cmt_txn);
		int cm = 0;
		if (cmt_txn.contains(":") || cmt_txn.contains("-") || cmt_txn.contains("="))
			return cm;
		try {
			cm = Integer.parseInt(cmt_txn);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cm;
	}

	public static void writeLatency() {

	}

	public static double[] getMinLatencyHostNo(Vector<Double> latencies) {
		double min = latencies.get(0);
		int index = 0;

		for (int i = 1; i < latencies.size(); i++) {
			if (min > latencies.get(i)) {
				min = latencies.get(i);
				index = i;
			}
		}
		System.out.println("index: " + index);
		System.out.println("min: " + min);

		return new double[] { index, min };
	}

	/*
	 * public static Vector<Metrics> fillOutMetrics2(int c, Vector<Metrics>
	 * clientMetrics) { Vector<Metrics> returnClientMetrics = new Vector<Metrics>();
	 * Vector<Double> finLatencies = new Vector<Double>(); Metrics metrics;
	 * List<String> aList; String client_folder_name = "Client" + c, file_name,
	 * final_fName; double min_fin_latency_value = -1; int min_fin_latency_host_no =
	 * -1;
	 * 
	 * for (int t = 1; t <= 35; t++) { metrics = new Metrics();
	 * 
	 * System.out.println("THIS IS ITERATION NUMBER " + t);
	 * 
	 * // 1. Find the host with minimum finalized latency for (int h = 0; h <
	 * hosts_IPS.size(); h++) {
	 * 
	 * file_name = "host_" + hosts_IPS.elementAt(h) + "_1_" + t; final_fName =
	 * results_path + client_folder_name + rates_folder_name + file_name; aList =
	 * readMetricsFile(final_fName); Double finLatency = 0.0; try { finLatency =
	 * readFinLatency(aList); } catch (Exception e) { e.printStackTrace(); }
	 * finLatencies.add(finLatency); }
	 * 
	 * // 2. get the host with minimum finalized latency double[] min_fin_late = new
	 * double[2]; min_fin_late = getMinLatencyHostNo(finLatencies);
	 * finLatencies.clear(); min_fin_latency_host_no = (int) min_fin_late[0];
	 * System.out.println("Host Index: " + min_fin_latency_host_no);
	 * min_fin_latency_value = min_fin_late[1];
	 * System.out.println("Finalized Delay: " + min_fin_latency_value);
	 * 
	 * // 3. set the host with minimum finalized latency
	 * metrics.setHostIndex(min_fin_latency_host_no);
	 * metrics.setFinalizedLatency(min_fin_latency_value);
	 * 
	 * // 4. set the host file to read metrics from file_name = "host_" +
	 * hosts_IPS.elementAt(min_fin_latency_host_no) + "_1_" + t; final_fName =
	 * results_path + client_folder_name + rates_folder_name + file_name;
	 * 
	 * aList = readMetricsFile(final_fName);
	 * 
	 * // 5. setCommitLatency metrics.setCommitLatency(readCmtLatency(aList));
	 * metrics.setEffectiveThroughput(readEffThroughput(aList));
	 * metrics.setOverallThroughput(readOvThroughput(aList));
	 * metrics.setNoFinalizedTransactions(readFinTxs(aList));
	 * metrics.setNoCommittedTransactions(readCmtTxs(aList));
	 * 
	 * returnClientMetrics.add(metrics); }
	 * 
	 * for (int i = 0; i < returnClientMetrics.size(); i++) { System.out.print((i +
	 * 1) + ": "); System.out.print("getHostIndex(): ");
	 * System.out.print(returnClientMetrics.get(i).getHostIndex());
	 * System.out.print(", getFinalizedLatency(): ");
	 * System.out.print(returnClientMetrics.get(i).getFinalizedLatency());
	 * System.out.print(", getCommitLatency(): ");
	 * System.out.print(returnClientMetrics.get(i).getCommitLatency());
	 * System.out.print(", getEffectiveThroughput(): ");
	 * System.out.print(returnClientMetrics.get(i).getEffectiveThroughput());
	 * System.out.print(", getOverallThroughput(): ");
	 * System.out.print(returnClientMetrics.get(i).getOverallThroughput());
	 * System.out.print(", getNoFinalizedTransactions(): ");
	 * System.out.print(returnClientMetrics.get(i).getNoFinalizedTransactions());
	 * System.out.print(", getNoCommittedTransactions(): ");
	 * System.out.print(returnClientMetrics.get(i).getNoCommittedTransactions());
	 * System.out.print(", getSuccessProbability(): ");
	 * System.out.print(returnClientMetrics.get(i).getSuccessProbability());
	 * System.out.println();
	 * 
	 * }
	 * 
	 * return returnClientMetrics; }
	 */

	public static Vector<Metrics> fillOutMetrics(int c, Vector<Metrics> clientMetrics) throws IOException {
		Vector<Metrics> returnClientMetrics = new Vector<Metrics>();
		Vector<Double> comLatencies = new Vector<Double>();
		Metrics metrics;
		List<String> aList;
		String client_folder_name = "Client" + c, file_name, final_fName;
		double min_com_latency_value = -1;
		int min_com_latency_host_no = -1;

		for (int t = 1; t <= 35; t++) 
		{
			int countForLostFiles = 0;
			metrics = new Metrics();
			System.out.println("THIS IS ITERATION NUMBER " + t);

			// 1. Find the host with minimum COMMIT latency
			for (int h = 0; h < hosts_IPS.size(); h++) 
			{
				file_name = "host_" + hosts_IPS.elementAt(h) + "_1_" + t;
				final_fName = results_path + client_folder_name + rates_folder_name + file_name;
				if (file_name.equals(null))
					continue;
				if (!canReadMetricsFile(final_fName)) 
				{
					System.out.println("CANNOT READ: " + final_fName);
					countForLostFiles++;
					continue;
				}

				aList = readMetricsFile(final_fName);
				Double comLatency = 0.0;
				try 
				{
					comLatency = readCmtLatency(aList);
					if (comLatency == 500.0)
						countForLostFiles++;
				} catch (Exception e) 
				{
					e.printStackTrace();
				}
				comLatencies.add(comLatency);
			}

			if (countForLostFiles == hosts_IPS.size()) 
			{
				metrics.setCommitLatency(0.0);
				metrics.setFinalizedLatency(0);
				metrics.setEffectiveThroughput(0.0);
				metrics.setOverallThroughput(0.0);
				metrics.setNoFinalizedTransactions(0);
				metrics.setNoCommittedTransactions(0);
				metrics.setHostIndex(0);
				comLatencies.clear();

			} 
			
			else 
			{
				// 2. get the host with minimum COMMIT latency
				double[] min_com_late = new double[2];
				System.out.println("comLatencies.size(): "+comLatencies.size());
				min_com_late = getMinLatencyHostNo(comLatencies);
				comLatencies.clear();
				min_com_latency_host_no = (int) min_com_late[0];
				System.out.println("Host Index: " + min_com_latency_host_no);
				min_com_latency_value = min_com_late[1];
				System.out.println("Commit Delay: " + min_com_latency_value);

				// 3. set the host with minimum finalized latency
				metrics.setHostIndex(min_com_latency_host_no);
				metrics.setFinalizedLatency(min_com_latency_value);

				// 4. set the host file to read metrics from
				file_name = "host_" + hosts_IPS.elementAt(min_com_latency_host_no) + "_1_" + t;
				final_fName = results_path + client_folder_name + rates_folder_name + file_name;

				aList = readMetricsFile(final_fName);

				// 5. setCommitLatency
				metrics.setCommitLatency(readCmtLatency(aList));
				metrics.setEffectiveThroughput(readEffThroughput(aList));
				metrics.setOverallThroughput(readOvThroughput(aList));
				metrics.setNoFinalizedTransactions(readFinTxs(aList));
				metrics.setNoCommittedTransactions(readCmtTxs(aList));
			}
			
			returnClientMetrics.add(metrics);
		}

		for (int i = 0; i < returnClientMetrics.size(); i++) {
			System.out.print((i + 1) + ": ");
			System.out.print("getHostIndex(): ");
			System.out.print(returnClientMetrics.get(i).getHostIndex());
			System.out.print(", getFinalizedLatency(): ");
			System.out.print(returnClientMetrics.get(i).getFinalizedLatency());
			System.out.print(", getCommitLatency(): ");
			System.out.print(returnClientMetrics.get(i).getCommitLatency());
			System.out.print(", getEffectiveThroughput(): ");
			System.out.print(returnClientMetrics.get(i).getEffectiveThroughput());
			System.out.print(", getOverallThroughput(): ");
			System.out.print(returnClientMetrics.get(i).getOverallThroughput());
			System.out.print(", getNoFinalizedTransactions(): ");
			System.out.print(returnClientMetrics.get(i).getNoFinalizedTransactions());
			System.out.print(", getNoCommittedTransactions(): ");
			System.out.print(returnClientMetrics.get(i).getNoCommittedTransactions());
			System.out.print(", getSuccessProbability(): ");
			System.out.print(returnClientMetrics.get(i).getSuccessProbability());
			System.out.println();

		}

		return returnClientMetrics;
	}

}
