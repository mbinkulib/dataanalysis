package DataAnalysis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
//import java.util.Vector;

public class ReadEffThroughput {

	// static String rates_folder_name;
	// static Vector<String> hosts_IPS;

	public static void readEffectiveThrougput(int s, int r) throws IOException {

		File effectiveThroughputFile1, effectiveThroughputFile2, effectiveThroughputFile3, effectiveThroughputFile4;
		FileOutputStream FOSeffectiveThroughput1, FOSeffectiveThroughput2, FOSeffectiveThroughput3,
				FOSeffectiveThroughput4;
		BufferedWriter BWeffectiveThroughput1 = null, BWeffectiveThroughput2 = null, BWeffectiveThroughput3 = null,
				BWeffectiveThroughput4 = null;

		// int[] host_sets = {12}; // {2, 4, 8, 12, 16};
		// int[] rates = {500};// {50, 500, 1000, 1500};

		// for (int s = 0; s < host_sets.length; s++)
		// {
		System.out.println("Set#: " + (s + 1) + ", set value is: " + s);
		// hosts_IPS = ExtractMetrics.readIPS(host_sets[s]);

		// for (int r = 0; r < rates.length; r++)
		// {
		String rates_folder_name = "/exp_" + s + "_servers_1_threads_" + r + "_rates_4_clients_flex/"; // host_128.110.153.127_4_5
		System.out.println(rates_folder_name);
		double avg = 0;
		// avg_all=0
		int devBy = 0; // int devBy = 35;

		effectiveThroughputFile1 = new File(
				ExtractMetrics.analysis_dir + rates_folder_name + "effectiveThroughputAv1_" + s + "_" + r + ".txt");
		FOSeffectiveThroughput1 = new FileOutputStream(effectiveThroughputFile1);
		BWeffectiveThroughput1 = new BufferedWriter(new OutputStreamWriter(FOSeffectiveThroughput1));

		effectiveThroughputFile2 = new File(
				ExtractMetrics.analysis_dir + rates_folder_name + "effectiveThroughputAv2_" + s + "_" + r + ".txt");
		FOSeffectiveThroughput2 = new FileOutputStream(effectiveThroughputFile2);
		BWeffectiveThroughput2 = new BufferedWriter(new OutputStreamWriter(FOSeffectiveThroughput2));

		effectiveThroughputFile3 = new File(
				ExtractMetrics.analysis_dir + rates_folder_name + "effectiveThroughputAv3_" + s + "_" + r + ".txt");
		FOSeffectiveThroughput3 = new FileOutputStream(effectiveThroughputFile3);
		BWeffectiveThroughput3 = new BufferedWriter(new OutputStreamWriter(FOSeffectiveThroughput3));

		effectiveThroughputFile4 = new File(
				ExtractMetrics.analysis_dir + rates_folder_name + "effectiveThroughputAv4_" + s + "_" + r + ".txt");
		FOSeffectiveThroughput4 = new FileOutputStream(effectiveThroughputFile4);
		BWeffectiveThroughput4 = new BufferedWriter(new OutputStreamWriter(FOSeffectiveThroughput4));

		System.out.println("Reading effective throughput");
		for (int i = 1; i <= 4; i++) {
			File file = new File(
					ExtractMetrics.analysis_dir + rates_folder_name + "effectiveThroughputClient" + (i) + ".txt");
			try {
				@SuppressWarnings("resource")
				BufferedReader br = new BufferedReader(new FileReader(file));
				String st;
				while ((st = br.readLine()) != null) 
				{
					if (Double.parseDouble(st) == 0.0)
						continue;
					System.out.println(st);
					devBy++;
					double val = Double.parseDouble(st);
					if (val > 0)
						avg = avg + val;
					// else
					// devBy --;
				}

				System.out.println("avg of " + i + "= " + (avg / devBy) + " which is devided by " + devBy);

				switch (i) {
				case 1:
					BWeffectiveThroughput1.write((avg / devBy) + "");
					BWeffectiveThroughput1.newLine();
					break;
				case 2:
					BWeffectiveThroughput2.write((avg / devBy) + "");
					BWeffectiveThroughput2.newLine();
					break;
				case 3:
					BWeffectiveThroughput3.write((avg / devBy) + "");
					BWeffectiveThroughput3.newLine();
					break;
				case 4:
					BWeffectiveThroughput4.write((avg / devBy) + "");
					BWeffectiveThroughput4.newLine();
					break;
				}

				// avg_all=avg_all+(avg/devBy);
				avg = 0;
				devBy = 0;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// devBy = 35;
		}
		// System.out.println("avg of all 4 = "+(avg_all/4));

		// }

		BWeffectiveThroughput1.close();
		BWeffectiveThroughput2.close();
		BWeffectiveThroughput3.close();
		BWeffectiveThroughput4.close();

		// hosts_IPS = null;
		// }

	}

}
