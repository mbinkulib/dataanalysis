package DataAnalysis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Vector;


public class TestRead {

	static String rates_folder_name;
	static Vector<String> hosts_IPS;
	
	@SuppressWarnings("resource")
	public static void main(String[] args) throws IOException {
		
		File finalizedLatencyFile;
		FileOutputStream FOSfinalizedLatency;
		BufferedWriter BWfinalizedLatency = null;
		
		int[] host_sets = {4}; // {2, 4, 8, 12, 16};
		int[] rates = {500};// {50, 500, 1000, 1500};

		for (int s = 0; s < host_sets.length; s++) 
		{
			System.out.println("Set#: " + (s + 1) + ", set value is: " + host_sets[s]);
			hosts_IPS = ExtractMetrics.readIPS(host_sets[s]);

			for (int r = 0; r < rates.length; r++) 
			{
				rates_folder_name = "/exp_" + host_sets[s] + "_servers_4_threads_" + rates[r] + "_rates/"; // host_128.110.153.127_4_5
				System.out.println(rates_folder_name);
				double avg=0, avg_all=0;
				int devBy = 0;
				
				finalizedLatencyFile = new File(ExtractMetrics.analysis_dir + rates_folder_name + "finalizedLatencySum_"+host_sets[s]+"_"+rates[r]+".txt");
				FOSfinalizedLatency = new FileOutputStream(finalizedLatencyFile);
				BWfinalizedLatency = new BufferedWriter(new OutputStreamWriter(FOSfinalizedLatency));
				
				
				System.out.println("Reading finalized latency");
				for (int i = 1; i <=4; i++) 
				{
					File file = new File(ExtractMetrics.analysis_dir + rates_folder_name + "finalizedLatencyClient"+(i)+".txt"); 
					try 
					{
					  @SuppressWarnings("resource")
					  BufferedReader br = new BufferedReader(new FileReader(file)); 
					  String st; 
					  while ((st = br.readLine()) != null) 
					  {
						    System.out.println(st);
						    double val = Double.parseDouble(st);
						    if (val > 0)
						    {
						    	avg=avg+val;
						    	devBy++;
						    }
						    //else
						    	//devBy --;
					  }
					  
					  System.out.println("avg of "+i+"= "+(avg/devBy)+" which is devided by "+devBy);
					  
					  avg_all=avg_all+(avg/devBy);
					  avg = 0;
					} 
					catch (IOException e) 
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					} 
					devBy = 0;
				} 
				System.out.println("avg of all  = "+(avg_all/4));
				BWfinalizedLatency.write((avg_all/4)+"");
				BWfinalizedLatency.newLine();
				
				
			}
			hosts_IPS = null;
		}
		
		BWfinalizedLatency.close();
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		// TODO Auto-generated method stub
		///home/blockchains/raft_4_results/blockbench/exp_2_servers_4_threads_10_rates
		
		/*
		 * int numLastLineToRead = 7; File file = new File(
		 * "/home/blockchains/eclipse-workspace/DataAnalysis/Results/Test/exp_2_servers_4_threads_10_rates/client_10.10.1.1_4_1"
		 * );
		 * 
		 * List<String> result = new ArrayList<>();
		 * 
		 * try (ReversedLinesFileReader reader = new ReversedLinesFileReader(file,
		 * StandardCharsets.UTF_8)) {
		 * 
		 * String line = ""; while ((line = reader.readLine()) != null && result.size()
		 * < numLastLineToRead) { System.out.println(line); result.add(line); }
		 * 
		 * } catch (IOException e) { e.printStackTrace(); }
		 * 
		 * // Read cmt_delay String cmt_delay =
		 * result.get(4).substring(result.get(4).indexOf('=')+2,
		 * result.get(4).indexOf('s')); System.out.println("Commit Delay: "+cmt_delay);
		 * 
		 * //Read fin_delay String fin_delay =
		 * result.get(3).substring(result.get(3).indexOf('=')+2,
		 * result.get(3).indexOf('s'));
		 * System.out.println("Finalized Delay: "+fin_delay);
		 * 
		 * int num_ov = result.get(0).indexOf('=')+1; String ov_throughput =
		 * result.get(0).substring(num_ov, num_ov+8);
		 * System.out.println("Overal Throughput: "+ov_throughput);
		 * 
		 * int num_eff = result.get(1).indexOf('=')+1; String eff_throughput =
		 * result.get(1).substring(num_eff+1, num_eff+8);
		 * System.out.println("Effective Throughput: "+eff_throughput);
		 * 
		 * 
		 * 
		 * String cmt_txn = result.get(5).substring(result.get(5).indexOf('=')+2,
		 * result.get(5).length());
		 * System.out.println("Number of committed transactions: "+cmt_txn);
		 * 
		 * String fin_txn = result.get(6).substring(result.get(6).indexOf('=')+2,
		 * result.get(6).length());
		 * System.out.println("Number of finalized transactions: "+fin_txn);
		 * 
		 * System.out.
		 * println("Success Probability: Number of committed transactions / Number of submitted transactions * 100"
		 * ); System.out.println("Success Probability: "); int prob =
		 * Integer.parseInt(cmt_txn); double prob_result = 1000/prob*100;
		 * System.out.print(prob_result);
		 *///(Integer.parseInt(cmt_txn)/1000*100)
        
        
        
        
		/*for (int i=1; i<=35; i++)
		{
			new File("/home/blockchains/eclipse-workspace/DataAnalysis/2nodes/1500/"+i).mkdir();

		}*/
		//Vector<Double> cmtLatenciesFinal= new Vector<Double>();
      //cmtLatenciesFinal = new Vector<Double>();
        
      //System.out.println("For Client "+c+": ");
		//System.out.println("min_cmt_latency_host_no: "+min_fin_latency_host_no+" with min_cmt_latency_value: "+min_fin_latency_value);
		
		//cmtLatenciesFinal.add(Double.parseDouble(readCmtLatency(results_path + client_folder_name + rates_folder_name + "host_" + hosts_IPS.elementAt(min_fin_latency_host_no) + "_4_" + t)));
		
		//Calculate other metrics on the host who has the min commit latency
		/*String file_name = "host_" + hosts_IPS.elementAt(min_cmt_latency_host_no) + "_4_" + t;
		String final_fName = results_path + client_folder_name + rates_folder_name + file_name;
		
		File summaryFile = new File(analysis_dir+(hosts_IPS.elementAt(min_cmt_latency_host_no)+1)+".txt");
		FileOutputStream fosSummary = new FileOutputStream(summaryFile);
		BufferedWriter bwSummary = new BufferedWriter(new OutputStreamWriter(fosSummary));

		bwSummary.write("CommitLatency");
		bwSummary.write("\t");
		bwSummary.write("FinalizeLatency");
		bwSummary.write("\t");
		bwSummary.write("OverallThroughput");
		bwSummary.write("\t");
		bwSummary.write("EffectiveThroughput");
		bwSummary.write("\t");
		bwSummary.write("SuccessProbability");
		bwSummary.write("\t");
		bwSummary.newLine();


		
		//1. Find the finalized transactions latency
		Double FinLatency = Double.parseDouble(readFinLatency(final_fName));
		bwSummary.write("");
		bwSummary.newLine();
		
		//2. Find the effective throughput
		Double effThroughput = Double.parseDouble(readEffThroughput(final_fName));
		
		bwSummary.newLine();
		
		//3. Find the overall throughput
		Double ovThroughput = Double.parseDouble(readOvThroughput(final_fName));
		
		bwSummary.newLine();
		
		//4. Calculate success probability
		Double successProb = Double.parseDouble(readSuccessProb(final_fName));
		
		bwSummary.newLine();
		
		bwSummary.close();*/


	}

}
